package com.tinkoff.sirius.financial_tracker.services

import com.tinkoff.sirius.financial_tracker.entities.Transaction
import com.tinkoff.sirius.financial_tracker.entities.TransactionCategory
import com.tinkoff.sirius.financial_tracker.exceptions.ResourceNotFoundException
import com.tinkoff.sirius.financial_tracker.exceptions.SecurityViolationException
import com.tinkoff.sirius.financial_tracker.repositories.TransactionCategoryRepository
import org.springframework.stereotype.Service

@Service
class TransactionCategoryService(
    private val transactionCategoryRepository: TransactionCategoryRepository,
    private val userService: UserService
) {
    fun getAllTxCategories(): List<TransactionCategory> =
        transactionCategoryRepository.findAll().toList()

    fun getTxCategoriesForUserByEmail(email: String): List<TransactionCategory> {
        val userId = userService.findOrCreateUserByEmail(email).id!!
        return transactionCategoryRepository.getAllByUserIdOrUserId(userId)
    }
    /**
     * Returns existing category with the name of [transactionCategory] or creates a new one
     */
    fun createIfNotExists(transactionCategory: TransactionCategory, email: String? = null): TransactionCategory {
        if (email != null) {
            val userId = userService.findOrCreateUserByEmail(email).id!!
            transactionCategory.userId = userId
        }
        return transactionCategoryRepository.findByName(transactionCategory.name)
            ?: transactionCategoryRepository.save(transactionCategory)
    }

    fun deleteCategory(email: String, id: Int) {
        val realCategory: TransactionCategory = transactionCategoryRepository.getById(id) ?:
            throw ResourceNotFoundException("Provided category not found")

        val userId = userService.findOrCreateUserByEmail(email).id
        if (realCategory.userId != userId)
            throw SecurityViolationException("Provided transaction does not belong to provided user")

        return transactionCategoryRepository.delete(realCategory)
    }
}