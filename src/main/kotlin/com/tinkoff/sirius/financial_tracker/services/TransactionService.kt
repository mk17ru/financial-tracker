package com.tinkoff.sirius.financial_tracker.services

import com.tinkoff.sirius.financial_tracker.converter.TransactionConverter
import com.tinkoff.sirius.financial_tracker.dto.NumberResultDto
import com.tinkoff.sirius.financial_tracker.dto.TransactionDto
import com.tinkoff.sirius.financial_tracker.entities.Transaction
import com.tinkoff.sirius.financial_tracker.exceptions.ResourceNotFoundException
import com.tinkoff.sirius.financial_tracker.repositories.TransactionRepository
import org.springframework.stereotype.Service
import java.time.LocalDateTime

@Service
class TransactionService(
    private val transactionRepository: TransactionRepository,
    private val transactionConverter: TransactionConverter
) {

    fun findTransactionById(id : Int): TransactionDto? {
        val transaction = transactionRepository.findTransactionById(id)
                            ?: throw ResourceNotFoundException("Not found transaction with id: $id")
        return transactionConverter.convert(transaction)
    }

    fun addTransaction(transaction: Transaction): Transaction =
        transactionRepository.save(transaction)


    fun addTransaction(transaction: TransactionDto): TransactionDto {
        val entity = transactionConverter.convert(transaction)
        val addedEntity = addTransaction(entity)
        return transactionConverter.convert(addedEntity)
    }

    fun sumTransactionAmountByWalletId(walletId : Int) : NumberResultDto {
        return NumberResultDto(transactionRepository.sumTransactionAmountByWalletId(walletId))
    }

    fun incomeTransactionAmountByWalletId(walletId: Int) : NumberResultDto {
        return NumberResultDto(transactionRepository.incomeTransactionAmountByWalletId(walletId))
    }

    fun expenditureTransactionAmountByWalletId(walletId: Int) : NumberResultDto {
        return NumberResultDto(transactionRepository.expenditureTransactionAmountByWalletId(walletId))
    }

    fun getTransactionsByWalletId(id: Int, lastId: Int?, limit: Int): List<TransactionDto> {
        if (lastId == null) {
            return transactionRepository.getFirstTransactionPage(id, limit).map(transactionConverter::convert)
        } else {
            val tx = transactionRepository.findTransactionById(lastId)
                ?: throw ResourceNotFoundException("No transaction $lastId found.")

            val date = tx.date
            return transactionRepository.getTransactionPage(id, date, limit).map(transactionConverter::convert)
        }
    }

    fun deleteTransactionById(id: Int) {
        transactionRepository.deleteTransactionById(id)
    }

    fun updateTransactionById(id: Int, transaction: TransactionDto): TransactionDto {
        val trans = transactionRepository.findTransactionById(id)
            ?: throw ResourceNotFoundException("Not found transaction with id: $id")
        trans.amount = transaction.amount
        trans.categoryId = transaction.categoryId
        trans.walletId = transaction.walletId
        return transactionConverter.convert(transactionRepository.save(trans))
    }

    fun deleteTransactionByWalletId(walletId: Int) {
        transactionRepository.deleteTransactionsByWalletId(walletId)
    }

}