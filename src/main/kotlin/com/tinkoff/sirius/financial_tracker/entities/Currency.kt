package com.tinkoff.sirius.financial_tracker.entities

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id

/**
 * DecimalDigits - amount of digits after the comma.
 * Example: 318.32 Rubles - 2 decimal digits after the comma
 */
@Entity
data class Currency (
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Int? = null,
    var shortName: String,
    var decimalDigits: Int
)
