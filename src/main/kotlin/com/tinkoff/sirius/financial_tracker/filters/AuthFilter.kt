package com.tinkoff.sirius.financial_tracker.filters

import com.tinkoff.sirius.financial_tracker.exceptions.AuthorizationException
import com.tinkoff.sirius.financial_tracker.services.GoogleAuthService
import org.springframework.boot.web.servlet.FilterRegistrationBean
import org.springframework.context.annotation.Bean
import org.springframework.stereotype.Component
import javax.servlet.*
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Component
class AuthFilter(private val googleAuthService: GoogleAuthService) : Filter {

    override fun doFilter(
        servletRequest: ServletRequest,
        servletResponse: ServletResponse,
        filterChain: FilterChain
    ) {

        val request = servletRequest as HttpServletRequest
        val response = servletResponse as HttpServletResponse
        // TODO move to const
        val idToken: String? = request.getHeader("X-Token")
        try {
            if (idToken != null) {
                // Throws AuthorizationException
                val email = googleAuthService.validateAndRetrieveEmail(idToken)
                // Set header to get it in controller
                val mutableRequest = MutableHttpServletRequest(request)
                mutableRequest.putHeader("X-Email", email)
                filterChain.doFilter(mutableRequest, response)
                return
            } else if (idToken == "test" || request.getHeader("X-Email") != null && request.getHeader("X-Email").contains("test")) {
                // Backdoor for testing without token - TODO delete it
                filterChain.doFilter(request, response)
            } else {
                throw AuthorizationException("X-Token is empty - id token was not passed")
            }
        } catch (ex: AuthorizationException) {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED, ex.message)
        }
    }

    //TODO move somewhere else
    @Bean
    fun authFilterRegistration(): FilterRegistrationBean<AuthFilter> {
        val registration: FilterRegistrationBean<AuthFilter> = FilterRegistrationBean()
        registration.setFilter(AuthFilter(googleAuthService))
        registration.addUrlPatterns("/wallets", "/wallets/*", "/categories", "/categories/*", "/transactions", "/transactions/*")
        registration.order = 1
        return registration
    }

}