package com.tinkoff.sirius.financial_tracker.repositories

import com.tinkoff.sirius.financial_tracker.entities.Currency
import org.springframework.data.repository.CrudRepository

interface CurrencyRepository : CrudRepository<Currency, Int> {
    fun getByShortName(shortName: String): Currency?
}