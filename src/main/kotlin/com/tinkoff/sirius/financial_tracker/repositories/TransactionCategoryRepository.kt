package com.tinkoff.sirius.financial_tracker.repositories

import com.tinkoff.sirius.financial_tracker.entities.TransactionCategory
import com.tinkoff.sirius.financial_tracker.entities.User
import org.springframework.data.repository.CrudRepository

interface TransactionCategoryRepository : CrudRepository<TransactionCategory, Int> {
    fun getById(id: Int): TransactionCategory?
    fun findByName(name: String): TransactionCategory?
    fun getAllByUserIdOrUserId(userId: Int, userId2: Int? = null): List<TransactionCategory>
}