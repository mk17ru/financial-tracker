package com.tinkoff.sirius.financial_tracker.controller

import com.tinkoff.sirius.financial_tracker.dto.TransactionDto
import com.tinkoff.sirius.financial_tracker.services.TransactionService
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.media.Content
import io.swagger.v3.oas.annotations.media.ExampleObject
import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.web.bind.annotation.*
import java.time.LocalDateTime
import javax.transaction.Transactional
import io.swagger.v3.oas.annotations.parameters.RequestBody as SwaggerRequestBody

@RestController
@RequestMapping("transactions")
@Tag(name = "Транзакции")
class TransactionController(private val transactionService: TransactionService) {

    @PostMapping
    @Operation(summary = "Добавить транзакцию")
    @SwaggerRequestBody(
        content = [Content(
            examples = [
                ExampleObject(
                    name = "Добавить доход",
                    value = """
{
  "walletId": 1,
  "categoryId": 2,
  "amount": 1337,
  "date": "2002-08-18 12:13:14.228"
}
            """
                ), ExampleObject(
                    name = "Добавить расход",
                    value = """
                {
  "walletId": 1,
  "categoryId": 7,
  "amount": -228
}
            """
                )
            ]
        )]
    )
    fun addTransaction(
        @RequestBody transaction: TransactionDto,
        @RequestHeader("X-Email", defaultValue = "test@test-mail.com") email: String
    ): TransactionDto {
        return transactionService.addTransaction(transaction)
    }

    // ToDo: Do response in case of not found
    @GetMapping("/{id}")
    fun getTransaction(@PathVariable id: Int,
                       @RequestHeader("X-Email", defaultValue = "test@test-mail.com") email: String): TransactionDto?
                                                                            = transactionService.findTransactionById(id)

    @Transactional
    @DeleteMapping("/{id}")
    fun deleteTransaction(@PathVariable id: Int,
                          @RequestHeader("X-Email", defaultValue = "test@test-mail.com") email: String) {
        transactionService.deleteTransactionById(id)
    }

    @Transactional
    @PatchMapping("/{id}")
    @Operation(summary = "Amount, walletId, categoryId change here")
    fun updateTransaction(
        @PathVariable id: Int,
        @RequestHeader("X-Email", defaultValue = "test@test-mail.com") email: String,
        @RequestBody transaction: TransactionDto
    ): TransactionDto {
        return transactionService.updateTransactionById(id, transaction)
    }

}