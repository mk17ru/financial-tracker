package com.tinkoff.sirius.financial_tracker.dto

data class WalletDto(
    var id: Int? = 0,
    var name: String,
    var currency: CurrencyDto,
    var balanceLimit: Int? = null,
    var income : Int? = null,
    var expenditure : Int? = null,
    var isHidden: Boolean = false
)